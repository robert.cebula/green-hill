package com.example.greenhill;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.common.Results;
import com.example.common.TimeUtils;

import java.io.IOException;

import static com.example.common.TimeUtils.getTimeString;

public class CompareActivity extends AppCompatActivity {

    private TableLayout tableLayout;

    private final ResultsManager resultsManager = new ResultsManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compare);

        tableLayout = findViewById(R.id.compare_activity_table);

        String firstFilename = getIntent().getStringExtra("first_filename");
        String secondFilename = getIntent().getStringExtra("second_filename");

        Log.d("COMPARE_ACTIVITY", "firstFilename: " + firstFilename);
        Log.d("COMPARE_ACTIVITY", "secondFilename: " + secondFilename);

        try {
            createTableLayout(firstFilename, secondFilename);
        } catch (IOException e) {
            Log.e("COMPARE_ACTIVITY", e.toString());
        }
    }

    private void createTableLayout(String firstFilename, String secondFilename) throws IOException {
        Results firstResult = resultsManager.readResult(firstFilename);
        ResultItem firstResultItem = ResultItem.fromFilename(firstFilename);

        Results secondResult = resultsManager.readResult(secondFilename);
        ResultItem secondResultItem = ResultItem.fromFilename(secondFilename);

        addRow("date", firstResultItem.dateToString(), secondResultItem.dateToString());
        addRow("time", firstResultItem.timeToString(), secondResultItem.timeToString());
        addRow(
                "overall time",
                getTimeString(firstResult.overallTimeInMs(), TimeUtils.TimeStringMode.ONLY_MINUS),
                getTimeString(secondResult.overallTimeInMs(), TimeUtils.TimeStringMode.ONLY_MINUS));
        addRow("avg heart\nrate", firstResult.averageHeartRate(), secondResult.averageHeartRate());

        int NUMBER_OF_SAMPLES = 10;
        for (int it = 0; it < NUMBER_OF_SAMPLES; ++it) {
            int promile = (it + 1) * 100;
            int percent = promile / 10;
            String firstResultTime = getTimeString(firstResult.getTimeMsForPromile(promile), TimeUtils.TimeStringMode.ONLY_MINUS);
            String secondResultTime = getTimeString(secondResult.getTimeMsForPromile(promile), TimeUtils.TimeStringMode.ONLY_MINUS);
            addRow(percent + "%", firstResultTime, secondResultTime);
        }
    }

    private void addRow(String text, int firstInt, int secondInt) {
        addRow(text, Integer.toString(firstInt),Integer.toString(secondInt));
    }

    private void addRow(String firstText, String secondText, String thirdText) {
        TableRow tableRow = new TableRow(this);

        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        tableRow.setLayoutParams(layoutParams);

        String[] texts = { firstText, secondText, thirdText };
        for (int it = 0; it < texts.length; ++it) {
            TextView textView = new TextView(this);
            textView.setText(texts[it]);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24);
            textView.setBackgroundResource(R.drawable.row_border);
            textView.setPadding(12, 6, 12, 6);
            if (firstText.contains("\n")) {
                textView.setMinLines(2);
            }
            tableRow.addView(textView, it);
        }

        tableLayout.addView(tableRow);
    }
}
