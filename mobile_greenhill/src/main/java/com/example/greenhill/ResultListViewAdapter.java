package com.example.greenhill;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.common.TimeUtils;

import java.util.List;

import static com.example.common.TimeUtils.getTimeString;

public class ResultListViewAdapter extends BaseAdapter {
    private final List<ResultItem> resultItemList;
    private final Context context;
    private static LayoutInflater inflater = null;

    public ResultListViewAdapter(List<ResultItem> resultItemList, Context context) {
        this.resultItemList = resultItemList;
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return resultItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return resultItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = inflater.inflate(R.layout.results_listview_item, null);

        ResultItem resultItem = resultItemList.get(position);

        TextView dateText = vi.findViewById(R.id.date_text);
        dateText.setText(resultItem.dateToString());

        TextView timeText = vi.findViewById(R.id.time_text);
        timeText.setText(resultItem.timeToString());

        TextView overallTimeText = vi.findViewById(R.id.overall_time_text);
        String timeString = getTimeString(resultItem.overallTimeMs(), TimeUtils.TimeStringMode.ONLY_MINUS);
        overallTimeText.setText(timeString);

        return vi;
    }
}
