package com.example.greenhill;

import android.os.Environment;
import android.util.Log;

import com.example.common.Results;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class ResultsManager {
    private static final String DIR_NAME = "green_hill_results";

    void saveResult(String filename, Results results) throws IOException {
        File dir = getPublicStorageDir();
        if (!dir.mkdirs()) {
            Log.e("RESULTS_MANAGER", "Directory " + dir.getAbsolutePath() + " not created");
        }
        Log.d("RESULTS_MANAGER", "dir.getAbsolutePath() : " + dir.getAbsolutePath());

        File file = new File(dir, filename);
        if (file.createNewFile()) {
            FileOutputStream fileOutputStream = new FileOutputStream(file);

            fileOutputStream.write(results.toString().getBytes(StandardCharsets.UTF_8));

            fileOutputStream.close();
        }
    }

    Results readResult(String filename) throws IOException {
        File dir = getPublicStorageDir();
        File file = new File(dir, filename);

        String fileContent = getFileContent(new FileInputStream(file));

        return Results.fromString(fileContent);
    }

    boolean removeResult(ResultItem itemToRemove) {
        File dir = getPublicStorageDir();
        File fileToRemove = new File(dir, itemToRemove.filename());
        return fileToRemove.delete();
    }

    List<ResultItem> readResults() {
        List<ResultItem> resultItems = new ArrayList<>();

        File dir = getPublicStorageDir();

        for (File file : Objects.requireNonNull(dir.listFiles())) {
            if (file.getName().endsWith("result")) {
                resultItems.add(ResultItem.fromFilename(file.getName()));
            }
        }

        return resultItems;
    }

    private File getPublicStorageDir() {
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), DIR_NAME);
    }

    private String getFileContent(FileInputStream fis) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(fis, StandardCharsets.UTF_8)))
        {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
            return sb.toString();
        }
    }
}
