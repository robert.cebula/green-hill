package com.example.greenhill;

import android.content.Intent;
import android.util.Log;

import com.example.common.Results;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MobileDataListener extends WearableListenerService {

    public static final String UPDATE_RESULTS_ACTION = "com.example.greenhill.update_results";

    private final ResultsManager resultsManager = new ResultsManager();

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d("MOBILE_DATA_LISTENER", "Data changed");

        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                // DataItem changed
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo("/data_from_wearable") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    String resultsString = dataMap.getString("data");
                    Results results = Results.fromString(resultsString);

                    String filename = constructFilename(results.overallTimeInMs());
                    try {
                        resultsManager.saveResult(filename, results);

                        sendBroadcast();
                    } catch (IOException e) {
                        Log.e("MOBILE_DATA_LISTENER", e.toString());
                    }
                }
            }
        }
    }

    private void sendBroadcast() {
        Intent local = new Intent();
        local.setAction(UPDATE_RESULTS_ACTION);
        this.sendBroadcast(local);
    }

    private String constructFilename(int overallTimeInMs) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date date = new Date();
        return dateFormat.format(date) + "_" + overallTimeInMs + "_result";
    }
}
