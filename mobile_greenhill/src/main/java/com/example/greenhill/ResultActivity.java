package com.example.greenhill;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.common.Results;
import com.example.common.TimeUtils;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.Wearable;

import java.io.IOException;

import static com.example.common.TimeUtils.getTimeString;

public class ResultActivity extends AppCompatActivity {

    private TextView overallTimeTextView;
    private TextView dateTextView;
    private TextView averageHeartRateTextView;
    private TextView timeTextView;

    private ListView resultsListView;
    private ArrayAdapter<String> resultsListViewAdapter;
    private final ResultsManager resultsManager = new ResultsManager();

    private Results results;
    private DataClient dataClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        overallTimeTextView = findViewById(R.id.result_overall_time_text_view);
        dateTextView = findViewById(R.id.result_date_text_view);
        timeTextView = findViewById(R.id.result_time_text_view);
        averageHeartRateTextView = findViewById(R.id.average_heart_rate_text_view);
        resultsListView = findViewById(R.id.result_activity_results_list_view);

        String filename = getIntent().getStringExtra("filename");
        Log.d("RESULT_ACTIVITY", "Started with filename: " + filename);

        try {
            results = resultsManager.readResult(filename);
        } catch (IOException e) {
            Log.e("RESULT_ACTIVITY", e.toString());
            throw new RuntimeException(e);
        }
        overallTimeTextView.setText("Overall time: " + getTimeString(results.overallTimeInMs(), TimeUtils.TimeStringMode.ONLY_MINUS));
        averageHeartRateTextView.setText("Average heart rate: " + results.averageHeartRate());

        assert filename != null;
        ResultItem resultItem = ResultItem.fromFilename(filename);
        dateTextView.setText("Date: " + resultItem.dateToString());
        timeTextView.setText("Time: " + resultItem.timeToString());

        int NUMBER_OF_SAMPLES = 10;
        String[] resultsArray = new String[NUMBER_OF_SAMPLES];
        for (int it = 0; it < NUMBER_OF_SAMPLES; ++it) {
            String time = getTimeString(results.getTimeMsForPromile((it + 1) * 100), TimeUtils.TimeStringMode.ONLY_MINUS);
            resultsArray[it] = ((it + 1) * 10) + "%: " + time;
        }
        resultsListViewAdapter = new ArrayAdapter<>(this, R.layout.result_activity_results_list_view_item, resultsArray);
        resultsListView.setAdapter(resultsListViewAdapter);

        dataClient = Wearable.getDataClient(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.results_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_send_to_wearable) {
            Log.d("MOBILE", "Sending result to wearable");
            new SendDataToWearableTask(dataClient, results.toString()).execute();
            Toast.makeText(this, "Sent new best result to wearable", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
}
