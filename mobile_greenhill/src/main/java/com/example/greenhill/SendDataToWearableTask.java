package com.example.greenhill;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;

class SendDataToWearableTask extends AsyncTask<Void, Void, Void> {
    private final String data;
    private final DataClient dataClient;

    SendDataToWearableTask(DataClient dataClient, String data) {
        this.data = data;
        this.dataClient = dataClient;
    }

    @Override
    protected Void doInBackground(Void... args) {
        sendDataToWearable(data);
        return null;
    }

    @WorkerThread
    private void sendDataToWearable(String data) {
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/data_from_mobile");
        putDataMapReq.getDataMap().putString("data", data);
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        Task<DataItem> putDataTask = dataClient.putDataItem(putDataReq);
        Log.d("MOBILE", "try to send data to wearable");

        putDataTask.addOnSuccessListener(new OnSuccessListener<DataItem>() {
            @Override
            public void onSuccess(DataItem dataItem) {
                Log.d("MOBILE", "successfully send data to wearable");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("MOBILE", "failed sending data to wearable");
            }
        });
    }
}

