package com.example.greenhill;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.example.greenhill.MobileDataListener.UPDATE_RESULTS_ACTION;

public class MainMobileActivity extends AppCompatActivity {
    private BroadcastReceiver updateUIReciver;

    private ListView resultsListView;
    private BaseAdapter resultsListViewAdapter;
    private List<ResultItem> resultItems;

    private List<ResultItem> selectedItems = new ArrayList<>();

    private final ResultsManager resultsManager = new ResultsManager();

    private MenuItem deleteMenuItem;
    private MenuItem compareMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mobile);

        resultsListView = findViewById(R.id.results_list_view);
        resultsListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        createResultsListViewAdapter();

        updateResults();

        resultsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String filename = resultItems.get(position).filename();
                Log.d("MOBILE", "Clicked: " + filename);

                // we need this because it seems onItemClick by default changes that the item is clicked
                // and we want to handle our own logic of that
                resultsListView.setItemChecked(position, !resultsListView.isItemChecked(position));

                if (selectedItems.size() > 0) {
                    checkOrUncheckItemAtPosition(view, position);
                    onSelectedItemsChange();
                    return;
                }

                if (resultsListView.isItemChecked(position)) {
                    view.setBackgroundColor(Color.WHITE);
                    resultsListView.setItemChecked(position, false);
                    onSelectedItemsChange();
                    return;
                }

                startResultActivity(filename);
            }
        });

        resultsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                String filename = resultItems.get(position).filename();
                Log.d("MOBILE", "Long clicked: " + filename);

                checkOrUncheckItemAtPosition(view, position);
                onSelectedItemsChange();

                return true;
            }
        });

        registerBroadcastsListener();
    }

    private void checkOrUncheckItemAtPosition(View view, int position) {
        if (resultsListView.isItemChecked(position)) {
            view.setBackgroundColor(Color.WHITE);
            resultsListView.setItemChecked(position, false);
        } else {
            view.setBackgroundColor(Color.GRAY);
            resultsListView.setItemChecked(position, true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterBroadcastsListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mobile_menu, menu);

        deleteMenuItem = menu.findItem(R.id.menu_delete);
        compareMenuItem = menu.findItem(R.id.menu_compare);

        onSelectedItemsChange();

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_delete) {
            Log.d("MOBILE", "Clicked delete button");
            onDeleteClicked();
        } else if (id == R.id.menu_compare) {
            Log.d("MOBILE", "Clicked compare button");
            onCompareClicked();
        }

        return super.onOptionsItemSelected(item);
    }

    private void onCompareClicked() {
        String firstFilename = selectedItems.get(0).filename();
        String secondFilename = selectedItems.get(1).filename();

        startCompareActivity(firstFilename, secondFilename);
    }

    private void onSelectedItemsChange() {
        Log.d("MOBILE", "onSelectedItemsChange()");
        selectedItems.clear();
        SparseBooleanArray checked = resultsListView.getCheckedItemPositions();

        for (int it = 0; it < checked.size(); ++it) {
            if (checked.valueAt(it)) {
                selectedItems.add(resultItems.get(checked.keyAt(it)));
            }
        }

        Log.d("MOBILE", "selectedItems: " + selectedItems);

        if (compareMenuItem != null) {
            compareMenuItem.setVisible(selectedItems.size() == 2);
        }
        if (deleteMenuItem != null) {
            deleteMenuItem.setVisible(selectedItems.size() > 0);
        }
    }

    private void onDeleteClicked() {
        for (ResultItem itemToRemove : selectedItems) {
            Log.d("MOBILE", "Removing: " + itemToRemove);

            if (resultsManager.removeResult(itemToRemove)) {
                resultItems.remove(itemToRemove);
            }
        }

        removeSelectionFromResultsListView();

        resultsListViewAdapter.notifyDataSetChanged();
    }

    private void removeSelectionFromResultsListView() {
        for (int resultsItemPosition = 0; resultsItemPosition < resultsListView.getCount(); ++resultsItemPosition) {
            resultsListView.setItemChecked(resultsItemPosition, false);
            View view = resultsListView.getChildAt(resultsItemPosition);
            if (view != null) {
                view.setBackgroundColor(Color.WHITE);
            }
        }

        onSelectedItemsChange();
    }

    private void registerBroadcastsListener() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UPDATE_RESULTS_ACTION);
        updateUIReciver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateResults();
            }
        };
        registerReceiver(updateUIReciver,filter);
    }

    private void updateResults() {
        List<ResultItem> resultItems = resultsManager.readResults();
        this.resultItems.clear();
        this.resultItems.addAll(resultItems);

        Collections.sort(this.resultItems);

        removeSelectionFromResultsListView();

        resultsListViewAdapter.notifyDataSetChanged();
    }

    private void createResultsListViewAdapter() {
        resultItems = new ArrayList<>();
        resultsListViewAdapter = new ResultListViewAdapter(resultItems, this);
        resultsListView.setAdapter(resultsListViewAdapter);
    }

    private void unregisterBroadcastsListener() {
        unregisterReceiver(updateUIReciver);
    }

    private void startResultActivity(String filename) {
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("filename", filename);
        startActivity(intent);
    }

    private void startCompareActivity(String firstFilename, String secondFilename) {
        Intent intent = new Intent(this, CompareActivity.class);
        intent.putExtra("first_filename", firstFilename);
        intent.putExtra("second_filename", secondFilename);
        startActivity(intent);
    }
}
