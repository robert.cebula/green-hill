package com.example.greenhill;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.common.TimeUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static com.example.common.TimeUtils.getTimeString;

public class ResultItem implements Comparable<ResultItem> {
    private final String filename;
    private final Date date;
    private final int overallTimeMs;

    private ResultItem(String filename, Date date, int overallTimeMs) {
        this.filename = filename;
        this.date = date;
        this.overallTimeMs = overallTimeMs;
    }

    static ResultItem fromFilename(String filename) {
        String[] splitted = filename.split("_");
        Log.d("RESULT_ITEM", "splitted: " + Arrays.toString(splitted));

        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        String dateString = splitted[0] + "_" + splitted[1] + "_" + splitted[2] + "_" + splitted[3]
                + "_" + splitted[4] + "_" + splitted[5];
        Date date;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            Log.e("RESULT_ITEM", e.toString());
            throw new RuntimeException(e);
        }

        int overallTimeMs = Integer.parseInt(splitted[6]);

        return new ResultItem(filename, date, overallTimeMs);
    }

    @Override
    public String toString() {
        return dateToString() + " " + timeToString() + ": " +
                getTimeString(overallTimeMs, TimeUtils.TimeStringMode.ONLY_MINUS);
    }

    String filename() {
        return filename;
    }

    int overallTimeMs() {
        return overallTimeMs;
    }

    Date date() {
        return date;
    }

    String dateToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date);
    }

    String timeToString() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(date);
    }

    @Override
    public int compareTo(@NonNull  ResultItem otherItem) {
        return Integer.compare(this.overallTimeMs, otherItem.overallTimeMs);
    }
}
