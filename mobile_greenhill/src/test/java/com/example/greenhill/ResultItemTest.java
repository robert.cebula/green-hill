package com.example.greenhill;

import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;

public class ResultItemTest {
    @Test
    public void testFromFilename() {
        // given
        String filename = "2019_08_06_19_21_13_14524_result";

        // when
        ResultItem resultItem = ResultItem.fromFilename(filename);

        // then
        assertEquals(filename, resultItem.filename());
        assertEquals(14524, resultItem.overallTimeMs());
        assertEquals(new Date(2019, 8, 6, 19, 21, 13), resultItem.date());
    }
}