package com.example.greenhill;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class AverageHeartRateCalculatorTest {
    @Test
    public void testAverageHeartRateCalculatorThreeSamples() {
        // given
        AverageHeartRateCalculator averageHeartRateCalculator = new AverageHeartRateCalculator();

        // when
        averageHeartRateCalculator.setCurrentHeartRate(80);
        averageHeartRateCalculator.takeSample();
        averageHeartRateCalculator.setCurrentHeartRate(90);
        averageHeartRateCalculator.takeSample();
        averageHeartRateCalculator.takeSample();
        averageHeartRateCalculator.setCurrentHeartRate(70);

        // then
        int averageHeartRate = averageHeartRateCalculator.getAverageHeartRate();
        int expected = 87;
        assertEquals(expected, averageHeartRate);
    }

    @Test
    public void testAverageHeartRateCalculatorOneHundredSamples() {
        // given
        AverageHeartRateCalculator averageHeartRateCalculator = new AverageHeartRateCalculator();

        // when
        for (int it = 0; it < 100; ++it) {
            averageHeartRateCalculator.setCurrentHeartRate(it);
            averageHeartRateCalculator.takeSample();
        }

        // then
        int averageHeartRate = averageHeartRateCalculator.getAverageHeartRate();
        int expected = 50;
        assertEquals(expected, averageHeartRate);
    }
}