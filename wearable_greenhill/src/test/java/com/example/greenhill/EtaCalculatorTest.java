package com.example.greenhill;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class EtaCalculatorTest {
    @Test
    public void testEtaCalculator25Percent() {
        // given
        EtaCalculator etaCalculator = new EtaCalculator();
        int previousTotalInMs = 110;

        // when
        int eta = etaCalculator.calculateEtaInMs(previousTotalInMs, -5, 250, 1000);

        // then
        int expected = 90;
        assertEquals(expected, eta);
    }

    @Test
    public void testEtaCalculator50Percent() {
        // given
        EtaCalculator etaCalculator = new EtaCalculator();
        int previousTotalInMs = 140;

        // when
        int eta = etaCalculator.calculateEtaInMs(previousTotalInMs, 10, 500, 1000);

        // then
        int expected = 160;
        assertEquals(expected, eta);
    }

    @Test
    public void testEtaCalculator75Percent() {
        // given
        EtaCalculator etaCalculator = new EtaCalculator();
        int previousTotalInMs = 130;

        // when
        int eta = etaCalculator.calculateEtaInMs(previousTotalInMs, -15, 750, 1000);

        // then
        int expected = 110;
        assertEquals(expected, eta);
    }
}