package com.example.greenhill;

import android.content.Context;

import com.example.common.Results;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

class BestResultManager {

    private static final String BEST_RESULT_FILENAME = "best_result";
    private final Context context;

    private Results bestResults = null;

    BestResultManager(Context context) {
        this.context = context;
    }

    boolean hasBestResult() {
        return bestResults != null;
    }

    Results getBestResult() {
        return bestResults;
    }

    private String getFileContent(FileInputStream fis) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(fis, StandardCharsets.UTF_8)))
        {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
            return sb.toString();
        }
    }

    void init() {
        try {
            FileInputStream fileInputStream = context.openFileInput(BEST_RESULT_FILENAME);
            bestResults = Results.fromString(getFileContent(fileInputStream));
        } catch (IOException ex) {
            bestResults = null;
        }
    }

    void saveAsNewBestResultIfBetterThenPrevious(Results results) throws IOException {
        if (bestResults != null && bestResults.isBetter(results)) {
            return;
        }

        saveNewBestResult(results);
    }

    void saveNewBestResult(Results newBestResults) throws IOException {
        FileOutputStream fileOutputStream = context.openFileOutput(BEST_RESULT_FILENAME, Context.MODE_PRIVATE);
        fileOutputStream.write(newBestResults.toString().getBytes(StandardCharsets.UTF_8));
        fileOutputStream.close();
    }
}
