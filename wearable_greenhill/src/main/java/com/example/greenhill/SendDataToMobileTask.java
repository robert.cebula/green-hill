package com.example.greenhill;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;

class SendDataToMobileTask extends AsyncTask<Void, Void, Void> {
    private final String data;
    private final DataClient dataClient;

    SendDataToMobileTask(DataClient dataClient, String data) {
        this.data = data;
        this.dataClient = dataClient;
    }

    @Override
    protected Void doInBackground(Void... args) {
        sendDataToMobile(data);
        return null;
    }

    @WorkerThread
    private void sendDataToMobile(String data) {
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/data_from_wearable");
        putDataMapReq.getDataMap().putString("data", data);
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        Task<DataItem> putDataTask = dataClient.putDataItem(putDataReq);
        Log.d("WEARABLE", "try to send data to mobile");

        putDataTask.addOnSuccessListener(new OnSuccessListener<DataItem>() {
            @Override
            public void onSuccess(DataItem dataItem) {
                Log.d("WEARABLE", "successfully send data to mobile");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("WEARABLE", "failed sending data to mobile");
            }
        });
    }
}
