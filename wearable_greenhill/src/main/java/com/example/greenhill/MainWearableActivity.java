package com.example.greenhill;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.common.Results;
import com.example.common.TimeUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.Wearable;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.common.TimeUtils.getTimeString;

public class MainWearableActivity extends WearableActivity implements SensorEventListener {

    // CONFIG
    public static final int EIGHT_MINUTES_IN_MS = 8 * 60 * 1000;
    private static final double END_LATITUDE = 50.12457D;
    private static final double START_LATITUDE = 50.10067D;
    public static final int NUMBER_OF_SAMPLES = 1001;
    // END OF CONFIG

    private enum Status {
        STARTED,
        STOPPED
    }

    private Status status = Status.STOPPED;

    private static final int REQUEST_PERMISSIONS = 1;

    private AverageHeartRateCalculator averageHeartRateCalculator = new AverageHeartRateCalculator();
    private SensorManager sensorManager;
    private Sensor heartRateSensor;

    private TextView distanceProgressText;
    private TextView timeText;
    private TextView diffrenceTimeText;
    private TextView heartRateText;
    private TextView etaText;

    private Button startStopButton;
    private Button configureButton;

    private static final double DISTANCE = END_LATITUDE - START_LATITUDE;

    private long startTimeMs;

    private Timer timer;

    private DataClient dataClient;

    private final Results results = new Results(System.currentTimeMillis(), NUMBER_OF_SAMPLES);

    private final BestResultManager bestResultManager = new BestResultManager(this);
    private EtaCalculator etaCalculator = new EtaCalculator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_wearable);

        distanceProgressText = findViewById(R.id.distance_progress_text);
        timeText = findViewById(R.id.time_text);
        diffrenceTimeText = findViewById(R.id.diffrence_time_text);
        heartRateText = findViewById(R.id.heart_rate_text);
        etaText = findViewById(R.id.eta_text);
        startStopButton = findViewById(R.id.start_stop_button);
        configureButton = findViewById(R.id.configure_button);

        // Enables Always-on
        setAmbientEnabled();

        if (checkPermissionsAndRequestIfNeeded()) {
            requestLocationUpdates();
        }

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (status == Status.STARTED) {
                    stop();
                } else if (status == Status.STOPPED) {
                    start();
                }
            }
        });

        configureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainWearableActivity.this, ConfigurationActivity.class);
                MainWearableActivity.this.startActivity(intent);
            }
        });

        dataClient = Wearable.getDataClient(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        listenToHeartRate();
    }

    @Override
    protected void onStop() {
        super.onStop();

        stopListenToHeartRate();
    }

    private void start() {
        startStopButton.setText("STOP");
        setEtaText("---");
        setDiffrenceTimeText("---");
        status = Status.STARTED;
        startTimeMs = System.currentTimeMillis();
        bestResultManager.init();
        averageHeartRateCalculator.clear();
        startTimer(1000);
    }

    private void stop() {
        startStopButton.setText("START");
        status = Status.STOPPED;
        stopTimer();
    }

    private void startTimer(int intervalMs) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (status.equals(Status.STARTED)) {
                            long elapsedTimeMs = System.currentTimeMillis() - startTimeMs;
                            String time = getTimeString(elapsedTimeMs, TimeUtils.TimeStringMode.ONLY_MINUS);
                            timeText.setText("\uD83D\uDD52 " + time);
                            averageHeartRateCalculator.takeSample();
                        }
                    }
                });
            }
        }, 0, intervalMs);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS) {
            requestLocationUpdates();
            listenToHeartRate();
        }
    }

    @SuppressLint("MissingPermission")
    protected void requestLocationUpdates() {

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        FusedLocationProviderClient fusedLocationProviderClient =
                new FusedLocationProviderClient(this);

        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }

                for (Location location : locationResult.getLocations()) {
                    updateDistanceProgress(location);
                }
            }
        };

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
    }

    private void updateDistanceProgress(Location location) {
        Double currentLatitude = location.getLatitude();

        Double madeDistance = (END_LATITUDE - currentLatitude) / DISTANCE;
        int madeDistancePromile = (int) Math.round(1000.0D - madeDistance * 1000.0D);

        int elapsedTimeMs = (int) (System.currentTimeMillis() - startTimeMs);

        if (status.equals(Status.STARTED) && madeDistancePromile >= 0) {
            results.addResult(madeDistancePromile, elapsedTimeMs);

            if (bestResultManager.hasBestResult()) {
                int diffrenceInMs = bestResultManager.getBestResult().diffrenceInMs(elapsedTimeMs, madeDistancePromile);
                showDiffrenceTimeText(diffrenceInMs);

                int previousTotalInMs = bestResultManager.getBestResult().overallTimeInMs();
                int etaInMs = etaCalculator.calculateEtaInMs(previousTotalInMs, diffrenceInMs, madeDistancePromile, NUMBER_OF_SAMPLES - 1);
                setEtaText(getTimeString(etaInMs, TimeUtils.TimeStringMode.ONLY_MINUS));
            }
        }

        NumberFormat formatter = new DecimalFormat("#0.00");
        double madeDistancePercent = madeDistancePromile * 0.1D;
        distanceProgressText.setText("\uD83D\uDEE3️ " + formatter.format(madeDistancePercent) + "%");

        if (status.equals(Status.STARTED) && madeDistancePercent > 100) {
            exceedsOneHundredPercent();
        }
    }

    private void exceedsOneHundredPercent() {
        Log.i("WEARABLE", "Distance is greater than 100%, stopping");
        status = Status.STOPPED;
        stop();
        vibrate();

        int overallTimeInMs = results.overallTimeInMs();
        timeText.setText("\uD83D\uDD52 " + getTimeString(overallTimeInMs, TimeUtils.TimeStringMode.ONLY_MINUS));

        if (overallTimeInMs > EIGHT_MINUTES_IN_MS) {
            try {
                sendResultsToMobile();
                Log.d("WEARABLE", "Try to save best results");
                bestResultManager.saveAsNewBestResultIfBetterThenPrevious(results);
            } catch (IOException e) {
                Log.e("WEARABLE", e.toString());
            }
        }
    }

    private void showDiffrenceTimeText(int diffrenceInMs) {
        String timeText = getTimeString(diffrenceInMs, TimeUtils.TimeStringMode.PLUS_OR_MINUS);
        setDiffrenceTimeText(timeText);
    }

    private void sendResultsToMobile() {
        new SendDataToMobileTask(dataClient, results.toString()).execute();
    }

    private void vibrate() {
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        long[] vibrationPattern = {0, 500, 50, 300};
        //-1 - don't repeat
        final int indexInPatternToRepeat = -1;
        assert vibrator != null;
        vibrator.vibrate(vibrationPattern, indexInPatternToRepeat);
    }

    private boolean checkPermissionsAndRequestIfNeeded() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.BODY_SENSORS) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.BODY_SENSORS},
                    REQUEST_PERMISSIONS);

            return false;
        }

        return true;
    }

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);

        startStopButton.setVisibility(View.GONE);
        configureButton.setVisibility(View.GONE);
    }

    @Override
    public void onExitAmbient() {
        super.onExitAmbient();

        startStopButton.setVisibility(View.VISIBLE);
        configureButton.setVisibility(View.VISIBLE);
    }

    private void listenToHeartRate() {
        sensorManager = ((SensorManager) getSystemService(SENSOR_SERVICE));
        heartRateSensor = sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);

        sensorManager.registerListener(this, heartRateSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    private void stopListenToHeartRate() {
        sensorManager.unregisterListener(this, heartRateSensor);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_HEART_RATE) {
            String heartRate = "" + (int)sensorEvent.values[0];
            String newText = "❤ " + heartRate;
            if (averageHeartRateCalculator.hasResult()) {
                newText += " (" + averageHeartRateCalculator.getAverageHeartRate() + ")";
            }
            averageHeartRateCalculator.setCurrentHeartRate(Integer.parseInt(heartRate));
            heartRateText.setText(newText);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    private void setEtaText(String newText) {
        etaText.setText("\uD83C\uDFC1 " + newText);
    }

    private void setDiffrenceTimeText(String newText) {
        diffrenceTimeText.setText("\uD83D\uDC7B " + newText);
    }
}
