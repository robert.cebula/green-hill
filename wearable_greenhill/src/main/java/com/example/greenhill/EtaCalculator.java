package com.example.greenhill;

class EtaCalculator {
    int calculateEtaInMs(int previousTotalInMs,
                         int diffrenceInMs,
                         int madeDistance,
                         @SuppressWarnings("SameParameterValue") int totalDistance) {
        double factor = (double)totalDistance / (double)madeDistance;
        int profitToTheEnd = (int)(diffrenceInMs * factor);

        return previousTotalInMs + profitToTheEnd;
    }
}
