package com.example.greenhill;

import android.util.Log;

import com.example.common.Results;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.IOException;

public class WearableDataListener extends WearableListenerService {
    private final BestResultManager bestResultManager = new BestResultManager(this);

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d("WEARABLE_DATA_LISTENER", "Data changed");

        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo("/data_from_mobile") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    String newBestResultString = dataMap.getString("data");

                    Results newBestResult = Results.fromString(newBestResultString);
                    try {
                        bestResultManager.saveNewBestResult(newBestResult);
                    } catch (IOException e) {
                        Log.e("WEARABLE_DATA_LISTENER", e.toString());
                    }
                }
            }
        }

    }
}
