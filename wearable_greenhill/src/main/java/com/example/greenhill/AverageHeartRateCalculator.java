package com.example.greenhill;

class AverageHeartRateCalculator {
    private int currentHeartRate;
    private double currentAverageHeartRate;
    private int numberOfSamples;

    AverageHeartRateCalculator() {
        this.currentHeartRate = 0;
        clear();
    }

    int getAverageHeartRate() {
        return (int) Math.round(currentAverageHeartRate);
    }

    void setCurrentHeartRate(int currentHeartRate) {
        this.currentHeartRate = currentHeartRate;
    }

    void clear() {
        this.numberOfSamples = 1;
        this.currentAverageHeartRate = 0;
    }

    void takeSample() {
        currentAverageHeartRate +=
                (((double)currentHeartRate - currentAverageHeartRate) / (double)numberOfSamples);
        ++numberOfSamples;
    }

    boolean hasResult() {
        return numberOfSamples > 1;
    }
}
