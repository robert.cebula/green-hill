package com.example.greenhill;

import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.common.Results;
import com.example.common.TimeUtils;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.Wearable;

import java.util.Random;

import static com.example.common.TimeUtils.getTimeString;
import static com.example.greenhill.MainWearableActivity.NUMBER_OF_SAMPLES;

public class ConfigurationActivity extends WearableActivity {

    private DataClient dataClient;

    private Button sendFakeDataButton;
    private TextView bestResultOverallTimeText;

    private final BestResultManager bestResultManager = new BestResultManager(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        sendFakeDataButton = findViewById(R.id.send_fake_data_button);
        bestResultOverallTimeText = findViewById(R.id.best_result_overall_time_text);

        sendFakeDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendFakeResultsToMobile();
            }
        });

        dataClient = Wearable.getDataClient(this);

        bestResultManager.init();
        if (bestResultManager.hasBestResult()) {
            int bestResultOverallTimeInMs = bestResultManager.getBestResult().overallTimeInMs();
            String bestResultText = getTimeString(bestResultOverallTimeInMs, TimeUtils.TimeStringMode.ONLY_MINUS);
            bestResultOverallTimeText.setText("Best result: " + bestResultText);
        }
    }

    private void sendFakeResultsToMobile() {
        Results fakeResults = new Results(System.currentTimeMillis(), NUMBER_OF_SAMPLES);

        int multiplier = randomNumber(1, 10);
        for (int it = 0; it < 1001; ++it) {
            fakeResults.addResult(it, it * multiplier);
        }
        fakeResults.setAverageHeartRate(randomNumber(50, 150));

        new SendDataToMobileTask(dataClient, fakeResults.toString()).execute();
    }

    private int randomNumber(@SuppressWarnings("SameParameterValue") int min,
                             @SuppressWarnings("SameParameterValue") int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }
}
