package com.example.common;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ResultsTest {
    private static final int NUMBER_OF_SAMPLES = 1001;

    @Test
    public void testStringConversions() {
        // given
        Integer[] integers = new Integer[NUMBER_OF_SAMPLES];
        for (int it = 0; it < NUMBER_OF_SAMPLES; ++it) {
            integers[it] = it;
        }

        long epoch = 214014L;
        int averageHeartRate = 134;
        Results results = new Results(epoch, averageHeartRate, integers);

        // when
        String resultedString = results.toString();
        Results resultsFromString = Results.fromString(resultedString);

        // then
        assertEquals(results, resultsFromString);
    }

    @Test
    public void testAddResult() {
        // given
        Integer[] expectedIntegers = new Integer[NUMBER_OF_SAMPLES];
        for (int it = 0; it < NUMBER_OF_SAMPLES - 5; ++it) {
            expectedIntegers[it] = it;
        }
        expectedIntegers[996] = 998;
        expectedIntegers[997] = 1001;
        expectedIntegers[998] = 1004;
        expectedIntegers[999] = 1007;
        expectedIntegers[1000] = 1008;

        long epoch = 129129521L;
        int averageHeartRate = 154;
        Results expectedResults = new Results(epoch, averageHeartRate, expectedIntegers);

        // when
        Results results = new Results(epoch, averageHeartRate, NUMBER_OF_SAMPLES);
        results.addResult(995, 995);
        results.addResult(999, 1006);
        results.addResult(1002, 1011);

        // then
        assertEquals(expectedResults, results);
    }
}