package com.example.common;

import androidx.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Results {
    private final long epoch;
    private int averageHeartRate;
    // 1000 index is 1000 promile (whole track)
    private final Integer[] timeInMilliseconds;

    private int lastUpdatedSample;

    private final int totalNumberOfSamples;

    public Results(long epoch, int totalNumberOfSamples) {
        this(epoch, 0, totalNumberOfSamples);
    }

    @VisibleForTesting
    Results(long epoch, int averageHeartRate, int totalNumberOfSamples) {
        this.epoch = epoch;
        this.averageHeartRate = averageHeartRate;

        this.totalNumberOfSamples = totalNumberOfSamples;
        this.lastUpdatedSample = 0;
        this.timeInMilliseconds = new Integer[totalNumberOfSamples];
        for (int it = 0; it < totalNumberOfSamples; ++it) {
            this.timeInMilliseconds[it] = 0;
        }
    }

    @VisibleForTesting
    Results(long epoch, int averageHeartRate, Integer[] results) {
        this.epoch = epoch;
        this.averageHeartRate = averageHeartRate;

        this.totalNumberOfSamples = results.length;
        this.timeInMilliseconds = new Integer[totalNumberOfSamples];
        System.arraycopy(results, 0, timeInMilliseconds, 0, totalNumberOfSamples);
        this.lastUpdatedSample = totalNumberOfSamples - 1;
    }

    public void setAverageHeartRate(int averageHeartRate) {
        this.averageHeartRate = averageHeartRate;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(epoch).append("\n");
        result.append(averageHeartRate).append("\n");

        for (int it = 0; it < totalNumberOfSamples; ++it) {
            result.append(timeInMilliseconds[it]).append("\n");
        }

        return result.toString();
    }

    public static Results fromString(String input) {
        List<Integer> results = new ArrayList<>();

        long epoch = 0;
        int averageHeartRate = 0;
        boolean firstLine = true;
        boolean secondLine = true;
        for (String line : input.split("\n")) {
            if (firstLine) {
                epoch = Long.parseLong(line);
                firstLine = false;
                continue;
            } else if (secondLine) {
                averageHeartRate = Integer.parseInt(line);
                secondLine = false;
                continue;
            }

            results.add(Integer.parseInt(line));
        }

        return new Results(epoch, averageHeartRate, results.toArray(new Integer[0]));
    }

    public int getTimeMsForPromile(int promile) {
        return timeInMilliseconds[promile];
    }

    public void addResult(int promile, int currentTimeMs) {
        if (promile < 0
                || lastUpdatedSample >= totalNumberOfSamples - 1
                || promile <= lastUpdatedSample) {
            return;
        }

        int lastUpdatedSampleTimeMs = timeInMilliseconds[lastUpdatedSample];
        int incrementPerSampleInMs = countIncrementPerSample(lastUpdatedSample, promile, currentTimeMs, lastUpdatedSampleTimeMs);

        int counter = 1;
        for (int it = lastUpdatedSample + 1; it <= Math.min(promile, totalNumberOfSamples - 1); ++it) {
            timeInMilliseconds[it] = lastUpdatedSampleTimeMs + (counter * incrementPerSampleInMs);
            counter++;
        }

        lastUpdatedSample = promile;
    }

    private int countIncrementPerSample(int lastUpdatedSample, int currentSample, int currentTimeMs, int lastUpdatedSampleTimeMs) {
        return (int) Math.round(((double)currentTimeMs - (double)lastUpdatedSampleTimeMs) / ((double)currentSample - (double)lastUpdatedSample));
    }

    private int checkPromileInRange(int promile) {
        if (promile < 0) {
            promile = 0;
        } else if (promile >= totalNumberOfSamples) {
            promile = totalNumberOfSamples - 1;
        }
        return promile;
    }

    public int diffrenceInMs(int otherTimeInMs, int promile) {
        promile = checkPromileInRange(promile);

        int currentTimeInMs = timeInMilliseconds[promile];
        return otherTimeInMs - currentTimeInMs;
    }

    public boolean isBetter(Results other) {
        return (this.timeInMilliseconds[totalNumberOfSamples - 1] <= other.timeInMilliseconds[totalNumberOfSamples - 1]);
    }

    public int overallTimeInMs() {
        return timeInMilliseconds[totalNumberOfSamples - 1];
    }

    public int averageHeartRate() {
        return averageHeartRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Results results = (Results) o;
        return epoch == results.epoch &&
                averageHeartRate == results.averageHeartRate &&
                Arrays.equals(timeInMilliseconds, results.timeInMilliseconds);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(epoch, averageHeartRate);
        result = 31 * result + Arrays.hashCode(timeInMilliseconds);
        return result;
    }
}
