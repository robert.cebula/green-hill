package com.example.common;

public class TimeUtils {
    public enum TimeStringMode {
        PLUS_OR_MINUS,
        ONLY_MINUS
    }

    public static String getTimeString(long timeMs, TimeStringMode timeStringMode) {
        String result = "";

        if (timeMs < 0) {
            result += "-";
        } else if (!timeStringMode.equals(TimeStringMode.ONLY_MINUS)) {
            result += "+";
        }

        int elapsedTimeSec = Math.abs((int) (timeMs / 1000));
        int minutes = elapsedTimeSec / 60;
        int seconds = elapsedTimeSec % 60;
        result += String.format("%02d", minutes) + ":" + String.format("%02d", seconds);

        return result;
    }
}
